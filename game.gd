extends Node2D

# Signals
signal player_action(action)

# Consts
const TIME_LIMIT = 60.0
const TimeLeftTooltipFormat = "Shearing in %fm%ds"
const FLOCK_SIZE = 18
const FLOCK_MIN_TIME = 5
const FLOCK_MAX_TIME = 20
const MIN_GRASS = 5
const MAX_GRASS = 9
const SUSPICION_AWAY_FROM_FLOCK = 0.015
# Member variables
#var screen_size
var time_to_shearing = TIME_LIMIT # Time left in seconds
var suspicion = 0
var max_suspicion = 100
var highest_suspicion = 0
var suspicion_loss = 1 # per second
var flock_timer = 0
var flock_destination = Vector2()
var process_logic = true

func _ready():
	randomize()
	get_node("HUD/TimeLeftBar").set_max(TIME_LIMIT)
	get_node("HUD/TimeLeftBar").set_value(TIME_LIMIT)
	get_node("HUD/SuspicionBar").set_max(max_suspicion)
	get_node("HUD/SuspicionBar").set_value(suspicion)
	get_node("HUD/ToolBar/RamButton").connect("pressed", self, "_on_ram_pressed")
	get_node("HUD/ToolBar/EatButton").connect("pressed", self, "_on_eat_pressed")
	var i = 0
	
	var sheep = preload("res://sheep.tscn")
	var grass = preload("res://grass.tscn")
	var pen_tl = get_node("Level/Pen/Spawn-TL")
	var pen_br = get_node("Level/Pen/Spawn-BR")
	while i < FLOCK_SIZE:
		var node = sheep.instance()
		node.set_pos(spawn_in_area(pen_tl, pen_br))
		node.add_to_group('flock')
		get_node("Actors").add_child(node)
		i += 1
	i = 0
	while i < int(rand_range(MIN_GRASS, MAX_GRASS)):
		var node = grass.instance()
		node.set_pos(spawn_in_area(pen_tl, pen_br))
		node.add_to_group('grass')
		get_node("Level").add_child(node)
		i += 1
	set_process(true)

func spawn_in_area(tl, br):
	var min_x = tl.get_global_pos().x
	var max_x = br.get_global_pos().x
	var min_y = tl.get_global_pos().y
	var max_y = br.get_global_pos().y
	return Vector2(rand_range(min_x, max_x), rand_range(min_y, max_y))

func _process(delta):
	if not process_logic:
		return
	time_to_shearing -= delta
	var time_left = get_node("HUD/TimeLeftBar")
	time_left.set_value(time_to_shearing)
	var time_left_minute = floor(time_to_shearing / 60)
	var time_left_second = time_to_shearing - (time_left_minute * 60)
	var tooltip = TimeLeftTooltipFormat % [time_left_minute, time_left_second]
	time_left.set_tooltip(tooltip)
	if time_to_shearing < 0:
		game_over("Too late : the shearing is upon us!")
	
	var d_suspicion = -delta * suspicion_loss
	if flock_timer > 0:
		var suspicion_per_pixel = SUSPICION_AWAY_FROM_FLOCK
		var d = get_node("Actors/player").get_global_pos().distance_to(flock_destination)
		var flock_distance = get_node("Actors/player").get_global_pos().distance_to(avg_flock_location())
		#print("Distance from point: ", d)
		#print("Distance from flock center: ", flock_distance)
		d_suspicion += suspicion_per_pixel * (flock_distance-200) * delta
	update_suspicion(d_suspicion)
	
	if flock_timer > 0:
		flock_timer -= delta
	if flock_timer < 0:
		flock(false)
		flock_timer = 0

	if int(time_to_shearing) % 15 == 0 && flock_timer == 0:
		#print("Starting flock")
		flock(true)
	
func _on_ram_pressed():
	emit_signal("player_action", "ram")

func _on_eat_pressed():
	emit_signal("player_action", "eat")

func update_suspicion(amount):
	suspicion += amount
	var suspicion_bar = get_node('HUD/SuspicionBar')
	suspicion_bar.set_value(suspicion)
	if (suspicion > highest_suspicion):
		highest_suspicion = suspicion
	if suspicion < 0:
		suspicion = 0
	if (suspicion > max_suspicion):
		game_over('You were too suspicious: the farmer came for you!')

func game_over(message, failure = true):
	var n = get_node("HUD/Panel/PostGameMessage/Label")
	if failure:
		n.set_text("Oh no!\n%s" %[message])
	else:
		n.set_text("YEAH!!!\n%s" %[message])
	get_node("HUD/Panel").show()
	get_node("Actors/player").set_fixed_process(false)
	process_logic = false
	flock(false)

func avg_flock_location():
	var p = null
	for i in get_tree().get_nodes_in_group('flock'):
		if p == null:
			p = i.get_global_pos()
			continue
		p += i.get_global_pos()
	return p / get_tree().get_nodes_in_group('flock').size()

func flock(enable = true):
	if not enable:
		#print("Flocking stopped")
		for i in get_tree().get_nodes_in_group('flock'):
			i.flock(null, false)
		return
	var p = randf()
	var i = 0
	var d = Vector2()
	var points = get_node("Level/FlockPoints").get_children();
	var point_count = get_node("Level/FlockPoints").get_child_count()
	#print(p)
	for point in points:
		if p >= (float(i)/float(point_count)) and p <= (float((i+1)) / float(point_count)):
			d = point.get_global_pos()
			flock_destination = d
			#print("Flocking to ", d)
		i += 1
	for i in get_tree().get_nodes_in_group('flock'):
		i.flock(d)
	flock_timer = rand_range(FLOCK_MIN_TIME, FLOCK_MAX_TIME)

func _on_Replay_released():
	#print('replay pressed')
	get_tree().change_scene("res://game.tscn")

func _on_Quit_released():
	#print('quit pressed')
	get_tree().quit()


func _on_Quit_pressed():
	_on_Quit_released()
	
func _on_Replay_pressed():
	_on_Replay_released()
