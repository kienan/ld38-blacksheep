extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

const SPEED_MAX = 200
const SPEED_MIN = 10
const MOVE_SPEED = 200
const ACTION_RANGE = 65
var velocity = Vector2()
var nearest_face_target = Dictionary()
var cooldowns = {
  'ram' : {
    'default': 1,
    'current': 0,
  },
  'eat' : {
    'default': 0.5,
    'current': 0,
  } 
}
var strength = 1
var turd_count = 0
var facing_collisions = Dictionary()

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node('/root').connect('player_action', self, '_on_player_action')
	set_fixed_process(true)

func _fixed_process(delta):
	# Check Inputs
	if (Input.is_action_pressed("ui_up")):
		velocity.y = -MOVE_SPEED
		set_rot(0)
	if (Input.is_action_pressed("ui_down")):
		velocity.y = MOVE_SPEED
		set_rot(deg2rad(180.0))
	if (Input.is_action_pressed("ui_left")):
		velocity.x = -MOVE_SPEED
		set_rot(deg2rad(90.0))
	if (Input.is_action_pressed("ui_right")):
		velocity.x = MOVE_SPEED
		set_rot(deg2rad(-90.0))
	
	# Set limits and move
	var motion = velocity * delta
	motion = move(motion)
	# Stop movement for the moment
	velocity = Vector2()
	var space_state = get_world_2d().get_direct_space_state()
	var facing = Vector2(0, 1).rotated(get_rot())
	#print(get_global_pos())
	#print(facing)
	facing_collisions = space_state.intersect_ray( get_global_pos(), facing * ACTION_RANGE, [ self ] )
	for cd in cooldowns:
		if cooldowns[cd]['current'] > 0:
			cooldowns[cd]['current'] -= delta
	# Collision handling

func _on_game_player_action( action ):
	#print('Received action: ', action)
	if _can_do_action(action):
		_do_action(action)

func _can_do_action(action):
	if not action in cooldowns:
		return true
	if (cooldowns[action]['current'] > 0):
		#print("Action %s cooldown left: %f" % [action, cooldowns[action]['current']])
		return false
	else:
		return true

func _do_action(action):
	#print('Doing: ', action)
	if action in cooldowns:
		cooldowns[action]['current'] = cooldowns[action]['default']
	if action == 'ram':
		_do_ram()
	elif action == 'eat':
		_do_eat()

func _do_ram():
	# Ram Distance?
	var bodies = get_node('ActionArea').get_overlapping_bodies()
	#print(bodies)
	for b in bodies:
		if b.has_method('damage'):
			b.damage(strength)
			if get_node('/root/game').has_method('update_suspicion'):
				get_node('/root/game').update_suspicion(5)

func _do_eat():
	var areas = get_node('ActionArea').get_overlapping_areas()
	#print(areas)
	for a in areas:
		var n = a.get_parent()
		if n.has_method('is_eatable'):
			if n.is_eatable():
				n.eat(self)
	

func _on_VisibilityNotifier2D_exit_screen():
	get_node('/root/game').game_over("You made it to sweet sweet freedom :)", false)


func _on_VisibilityNotifier2D_exit_viewport( viewport ):
	get_node('/root/game').game_over("You made it to sweet sweet freedom :)", false)
