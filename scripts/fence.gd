extends StaticBody2D

signal hp_changed(b)
signal hp_empty(b)

# Defaults
var max_health = 50
var min_health = 0
var health = 50

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	randomize()
	get_node('HealthBar').set_max(max_health)
	if randf() <= 0.1:
		set_hp(rand_range(max_health * 0.5, max_health))
	else:
		set_hp(max_health)
	pass

func heal(amount):
	change_hp(amount)

func damage(amount):
	change_hp(-amount)
	
func change_hp(amount):
	var previous_health = health
	var next_health = previous_health + amount
	if (next_health > max_health):
		next_health = max_health
	if (previous_health != next_health):
		set_hp(next_health)
		emit_signal('hp_changed', self)
	if (next_health <= min_health):
		emit_signal('hp_empty', self)
		queue_free()
	
func set_hp(h):
	health = h
	get_node('HealthBar').set_value(health)
	if (health < max_health):
		if (get_global_rot() != 0):
			get_node('HealthBar').set_rotation(-get_rot())
			get_node('HealthBar').set_pos(Vector2(-18,15))
		get_node('HealthBar').show()
	else:
		get_node('HealthBar').hide()
