Game made for [Ludum Dare 38](https://ldjam.com) around the theme "Small World"

Assets are licensed under Creative Commons Attribution-ShareAlike 4.0 Internal (CC-BY-SA 4.0). See assets/LICENSE.txt

Code and project files are licensed under GPLv3: see LICENSE.txt

Copyright 2017 Kienan Stewart
