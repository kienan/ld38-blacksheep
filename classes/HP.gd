extends Node

signal hp_empty(node)

# Defaults
var max_health = 50
var min_health = 0
var health = 50

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func heal(amount):
	change_hp(amount)

func damage(amount):
	change_hp(-amount)
	
func change_hp(amount):
	health += amount
	if (health > max_health):
		health = max_health
	if (health < min_health):
		emit_signal('hp_empty', self)